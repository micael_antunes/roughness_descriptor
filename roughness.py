# Python function to calculate roughness according to Vassilakis' model.

# To calculate the roughnes:

# 1 - Import the < roughness > function into pyhton module.

# 2 - Use the function < vassilakis > to calculate the roughness. 
# You must use as a entry the frequencies and magnitudes of a spectrogram.



import numpy as np
from itertools import combinations
from librosa.util import peak_pick
import librosa as lb

def sumVassilakis(freqs, amps):

  #f_stack = np.stack(np.meshgrid(freqs,freqs),-1).reshape(-1,2)[::,::-1]
  #a_stack = np.stack(np.meshgrid(amps,amps),-1).reshape(-1,2)[::,::-1]
  f_stack = np.array(list(combinations(freqs,2)))
  a_stack = np.array(list(combinations(amps,2)))
  if(len(f_stack) == 0):
    return 0
  b1, b2 = 3.5, 5.75
  xm = 0.24
  s1, s2 = 0.0207, 18.96
  s = xm / (s1 * np.min(f_stack, -1) + s2) 
  X = (a_stack[:,0]* a_stack[:,1]) ** 0.1 * 0.5
  Y = (2*a_stack[:,1]/np.sum(a_stack, -1)) ** 3.11
  Z = np.exp(-b1* s * np.abs(np.diff(f_stack)).squeeze()) -np.exp(-b2 * s * np.abs(np.diff(f_stack)).squeeze())
  return np.sum(X*Y*Z)

def  vassilakis(freqs,mags):

  p = lambda w: peak_pick(w, 2, 2, 5, 10, 0.01, 2)
  peak = [p(w) for w in lb.amplitude_to_db(mags,ref=np.max,amin=1e-8)]

  rough = []
  for frame in range(len(mags)):
    if peak[frame].any():
      f = freqs[frame][peak[frame]]
      a = mags[frame, peak[frame]]
      rough.append(sumVassilakis(f, a))
    else:
      rough.append(None)
    
  return(rough)